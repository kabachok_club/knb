from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class LobbyModel(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(User, related_name='owner')
    opponent = models.ForeignKey(User, related_name='opponent', null=True)
    status = models.CharField(max_length=50, default='waiting')

    def __str__(self):
        return self.name