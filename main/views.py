from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .models import LobbyModel
# Create your views here.

@login_required()
def index(request):
    username = request.user.username
    user_list = User.objects.all()
    open_lobbies = LobbyModel.objects.filter(status='waiting').exclude(owner=request.user)
    context = {
        'username': username,
        'user_list': user_list,
        'open_lobbies': open_lobbies,
    }
    return render(request, 'main/index.html', context)


@login_required()
def lobby(request, lobby_id):
    l = LobbyModel.objects.get(pk=lobby_id)
    context = {
        'lobby': l,
    }
    if l.owner == request.user:
        return render(request, 'main/lobby.html', context)
    else:
        l.opponent = request.user
        l.status = 'Ready to game'
        l.save()
        return render(request, 'main/lobby.html', context)


@login_required()
def create(request):
    lobby_name = request.POST['lobby_name']
    owner = request.user
    status = 'waiting'

    l = LobbyModel.objects.create(name=lobby_name, owner=owner, status=status)
    l.save()
    return HttpResponseRedirect(reverse('main:lobby', args=(l.id,)))
