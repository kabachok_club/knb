from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^lobby/(?P<lobby_id>[0-9]+)/$', views.lobby, name='lobby'),
    url(r'^lobby/create/$', views.create, name='create')
]