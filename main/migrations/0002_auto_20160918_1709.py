# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lobbymodel',
            name='opponent',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, related_name='opponent'),
        ),
        migrations.AlterField(
            model_name='lobbymodel',
            name='status',
            field=models.CharField(max_length=50, default='waiting'),
        ),
    ]
